"""This module contains similarity measures for interval type-2 fuzzy sets."""

from numpy import linspace, e
from decimal import Decimal
import copy

import global_settings as gs


def zeng_li(fs1, fs2):
    """Based on the average distance between the membership values."""
    result = 0
    # restrict the universe of discourse because the
    # measure doesn't follow the property overlapping.
    n = 0
    for x in gs.get_x_points():
        fs1_l, fs1_u = fs1.calculate_membership(x)
        fs2_l, fs2_u = fs2.calculate_membership(x)
        #if fs1_l > 0 or fs2_l > 0:  # ignore empty slices
        result += abs(fs1_l - fs2_l) + abs(fs1_u - fs2_u)
        n += 1
    result /= (2 * n)
    result = 1 - result
    return result.quantize(gs.DECIMAL_ROUNDING)


def bustince(fs1, fs2, t_norm_min=True):
    """Based on the inclusion of one fuzzy set within the other."""
    yl_ab = 1
    yl_ba = 1
    yu_ab = 1
    yu_ba = 1
    for x in gs.get_x_points():
        fs1_l, fs1_u = fs1.calculate_membership(x)
        fs2_l, fs2_u = fs2.calculate_membership(x)
        yl_ab = min(yl_ab, min(1 - fs1_l + fs2_l,
                               1 - fs1_u + fs2_u))
        yl_ba = min(yl_ba, min(1 - fs2_l + fs1_l,
                               1 - fs2_u + fs1_u))
        yu_ab = min(yu_ab, max(1 - fs1_l + fs2_l,
                               1 - fs1_u + fs2_u))
        yu_ba = min(yu_ba, max(1 - fs2_l + fs1_l,
                               1 - fs2_u + fs1_u))
    return min(yl_ab, yl_ba), min(yu_ab, yu_ba)


def jaccard(fs1, fs2):
    """Ratio between the intersection and union of the fuzzy sets."""
    top = 0
    bottom = 0
    for x in gs.get_x_points():
        fs1_l, fs1_u = fs1.calculate_membership(x)
        fs2_l, fs2_u = fs2.calculate_membership(x)
        top += min(fs1_u, fs2_u) + min(fs1_l, fs2_l)
        bottom += max(fs1_u, fs2_u) + max(fs1_l, fs2_l)
    return (top / bottom).quantize(gs.DECIMAL_ROUNDING)


def zheng(fs1, fs2):
    """Similar to jaccard; based on the intersection and union of the sets."""
    top_a = 0
    top_b = 0
    bottom_a = 0
    bottom_b = 0
    for x in gs.get_x_points():
        fs1_l, fs1_u = fs1.calculate_membership(x)
        fs2_l, fs2_u = fs2.calculate_membership(x)
        top_a += min(fs1_u, fs2_u)
        top_b += min(fs1_l, fs2_l)
        bottom_a += max(fs1_u, fs2_u)
        bottom_b += max(fs1_l, fs2_l)
    return (Decimal('0.5') * ((top_a / bottom_a) + (top_b / bottom_b))
            ).quantize(gs.DECIMAL_ROUNDING)


def vector(fs1, fs2):
    """Vector similarity based on the distance and similarity of shapes."""
    fs1_clone = copy.deepcopy(fs1)
    fs2_clone = copy.deepcopy(fs2)
    # for this library only, the mean is used to speed the process
    # cos is correct but also the same as mean in this case
    #fs1_clone_c = fs1_clone.calculate_overall_centre_of_sets()
    #fs2_clone_c = fs2_clone.calculate_overall_centre_of_sets()
    fs1_clone_c = fs1_clone.mf1.mean
    fs2_clone_c = fs2_clone.mf1.mean
    dist = fs1_clone_c - fs2_clone_c
    # align the centroid of fs2_clone with fs1_clone to compare shapes
    fs2_clone.mf1.shift_membership_function(dist)
    fs2_clone.mf2.shift_membership_function(dist)
    # find out the support of the union of the fuzzy sets
    # and weight the absolute distance by this support
    x_min = min(max(fs1_clone.uod[0], fs1_clone.mf1.x_min),
                max(fs1_clone.uod[0], fs1_clone.mf2.x_min),
                max(fs2_clone.uod[0], fs2_clone.mf1.x_min),
                max(fs2_clone.uod[0], fs2_clone.mf2.x_min))
    x_max = max(min(fs1_clone.uod[1], fs1_clone.mf1.x_max),
                min(fs1_clone.uod[1], fs1_clone.mf2.x_max),
                min(fs2_clone.uod[1], fs2_clone.mf1.x_max),
                min(fs2_clone.uod[1], fs2_clone.mf2.x_max))
    r = Decimal(4) / (x_max - x_min)
    proximity = pow(Decimal(e), -r * abs(dist))
    shape_difference = jaccard(fs1_clone, fs2_clone)
    return (shape_difference * proximity).quantize(gs.DECIMAL_ROUNDING)
