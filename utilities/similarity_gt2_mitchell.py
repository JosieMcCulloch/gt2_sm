import random
from collections import defaultdict
from numpy import linspace
from decimal import Decimal

import global_settings as gs

M = 10
N = 10
X_DISC = gs.global_x_disc

class _bespoke_fuzzy_set():
    def __init__(self, left_boundary, right_boundary):
        self.uod = [left_boundary, right_boundary]
        self.points = defaultdict(int)
        
    def calculate_membership(self, x):
        return self.points[x]

def _get_embedded_set(fs, X):
    """ Returns a single embedded set of fs, where X is a list of values
        in the universe of discourse. """
    result = _bespoke_fuzzy_set(fs.uod[0], fs.uod[1])
    r = Decimal(random.random())
    for x in X:
        lower, upper = fs.calculate_membership(x, min(fs.zlevel_coords))
        result.points[x] = (r * (upper - lower)) + lower
    return result

def _get_all_embedded_sets(fs, X):
    """ Returns m embedded sets of fs as a dict of dicts, where X is a list of values
        in the universe of discourse. """
    #Use M instead of seperately M and N, for now
    result = {}
    for m in range(M):
        result[m] = _get_embedded_set(fs, X)
    return result
    
def _get_weight(fs, embedded_set):
    """ Returns the weight of the given embedded fuzzy set of fs. """
    result = 1
    for x, y in embedded_set.points.iteritems():
        z = fs.calculate_secondary_membership(x, y)
        if y > 0:
            result = min(result, z)
    return result

def _get_all_weights(fs, all_embedded_sets):
    """ Returns a dict of all the weights of the given embedded fuzzy sets of fs. """
    result = {}
    for m in range(M):
        result[m] = _get_weight(fs, all_embedded_sets[m])
    return result
    
def _get_normalised_weight(fs1_weights, fs2_weights, m_index, n_index):
    """ Returns the normalised weight between the two fuzzy sets weights
        as given by _get_all_weights. """
    result = 0
    for m in range(M):
        for n in range(N):
            result += min(fs1_weights[m], fs2_weights[n])
    if result == 0: return 0
    return min(fs1_weights[m_index], fs2_weights[n_index]) / result


def jaccard(fs1_embedded, fs2_embedded, X1, X2):
    X = sorted(list(set(X1).union(set(X2))))
    top = 0
    bottom = 0
    for x in X:
        y1 = fs1_embedded.calculate_membership(x)
        y2 = fs2_embedded.calculate_membership(x)
        top += min(y1, y2)
        bottom += max(y1, y2)
    return top / bottom


def calculate_similarity(fs1, fs2):
    result = 0
    #X1 = linspace(fs1.x_min, fs2.x_max, X_DISC)
    #X2 = linspace(fs2.x_min, fs2.x_max, X_DISC)
    X = gs.get_x_points()
    fs1_embedded_sets = _get_all_embedded_sets(fs1, X)
    fs2_embedded_sets = _get_all_embedded_sets(fs2, X)
    fs1_weights = _get_all_weights(fs1, fs1_embedded_sets)
    fs2_weights = _get_all_weights(fs2, fs2_embedded_sets)
    for m in range(M):
        for n in range(N):
            sim = jaccard(fs1_embedded_sets[m], fs2_embedded_sets[n], X, X)
            w = _get_normalised_weight(fs1_weights, fs2_weights, m, n)
            result += sim * w
    return round(result, 4)
    
