from decimal import Decimal
import matplotlib.pyplot as plt
from collections import defaultdict
from scipy.stats import rankdata


from utilities.general_t2_fuzzy_set import GeneralT2FuzzySet
from utilities import similarity_gt2
from utilities import similarity_gt2_mitchell
from utilities import global_settings as gs


import get_sets


gs.global_uod = [0, 1]
gs.global_x_disc = 101
gs.global_alpha_disc = 10
#gs.global_zlevel_disc = 10


FUNCTIONS = [similarity_gt2.jaccard,
             similarity_gt2.hao_crisp,
             similarity_gt2.zhao_crisp,
             similarity_gt2.yang_lin,
             similarity_gt2.hung_yang,
             similarity_gt2_mitchell.calculate_similarity
             ]


def set_value(x):
    return Decimal(x).quantize(gs.DECIMAL_ROUNDING)


def similarities(sets, function):
    """Calculate the similarity between all pairs of sets using the function."""
    results = []
    total = len(sets)
    for i in range(total):
        print i
        for j in range(i, total):
            results.append(function(sets[i], sets[j]))
    return results


def save_all_results():
    """Save similarity results of all functions with the same fuzzy sets."""
    sets = get_sets.load_data('fuzzy_sets/sets_gt2.pickle')
    for f in FUNCTIONS:
        print f
        results = similarities(sets, f)
        #get_sets.save_data(results, 'results/gt2/' + f.__name__ + '.pickle')
        get_sets.save_data(results, 'results/gt2/' + f.__name__ + 'blah.pickle')


def plot_all_figures():
    """Save scatter plots comparing similarity function results."""
    fig = plt.figure()
    f1 = get_sets.load_data('results/gt2/jaccard.pickle')
    f2s = ['hao_crisp', 'zhao_crisp', 'yang_lin', 'hung_yang', 'mitchell']
    labels = ['hm', 'zxld', 'yl', 'hy', 'm']
    for i in range(len(f2s)):
        ax = fig.gca()
        fig.subplots_adjust(bottom=0.15)
        ax.set_xlabel(r'$s^{\mathrm{GT2}}_{\mathrm{j}}$', fontsize=22)
        f2 = get_sets.load_data('results/gt2/' + f2s[i] + '.pickle')
        ax.set_ylabel(r'$s^{\mathrm{GT2}}_{\mathrm{' + labels[i] + '}}$',
                      fontsize=22)
        plt.scatter(f1, f2)
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.savefig('results/gt2/' + f2s[i] + '.pdf')
        plt.clf()


if __name__ == '__main__':
    #save_all_results()
    plot_all_figures()
