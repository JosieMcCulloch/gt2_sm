gt2sm
===========

What is it?
-----------
The purpose of this library is to demonstrate how the results of different similarity measures on type-2 fuzzy sets differ. This aids in choosing which is the best. This library:

* generates synthetic interval and general type-2 fuzzy sets 
* runs different similarity measures on all pairs of generated fuzzy sets
* compares similarity measure results through pairwise visual plots.

csv files are provided for those who want to see the fuzzy sets used and the similarity measure results without running the code.

"fuzzy_sets/sets.csv" lists the means and standard deviations of the membership functions of the type-2 fuzzy sets. In the code, lower and upper membership functions of the fuzzy sets have the same means and standard deviations, but the lower membership function is given a height of 0.7 (the upper has a height of 1).

The csv files in the results folder list the similarity between each set and each other set (for a total of 30 sets). Note that similarity is symmetrical for all functions.


The Latest Version
------------------
The latest version can found at
https://bitbucket.org/JosieMcCulloch/gt2sm


Prerequisites
--------------
* python 2.7
* decimal
* matplotlib


Installation
------------
No installation is required.


Contacts
--------
If you wish to contact about queries or submit bug reports,
please contact the mail address: josie.mcculloch@nottingham.ac.uk.


License
-------
Copyright (C) 2018 Josie McCulloch

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
