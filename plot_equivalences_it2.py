from decimal import Decimal
import matplotlib.pyplot as plt
from scipy.stats import stats


from utilities import similarity_it2
from utilities import global_settings as gs
from utilities import visualisations


import get_sets


gs.global_uod = [0, 1]
gs.global_x_disc = 101
gs.global_alpha_disc = 100
gs.set_rounding(2)


FUNCTIONS = [similarity_it2.jaccard,
             similarity_it2.zeng_li,
             similarity_it2.zheng,
             similarity_it2.bustince,
             similarity_it2.vector
             ]

def set_value(x):
    return Decimal(x).quantize(gs.DECIMAL_ROUNDING)


def similarities(sets, function):
    results = []
    total = len(sets)
    for i in range(total):
        print i
        for j in range(i, total):
            results.append(function(sets[j], sets[i]))
    return results


def save_all_results():
    sets = get_sets.load_data('fuzzy_sets/sets_it2.pickle')
    for f in FUNCTIONS:
        results = similarities(sets, f)
        get_sets.save_data(results, 'results/it2/' + f.__name__ + '.pickle')


def plot_all_figures():
    fig = plt.figure()
    f1 = get_sets.load_data('results/it2/jaccard.pickle')
    f2s = ['zheng', 'zeng_li', 'zeng_li_2', 'vector']
    labels = ['zwzz', 'zl', 'zl2', 'wm']
    for i in range(len(f2s)):
        ax = fig.gca()
        fig.subplots_adjust(bottom=0.15)
        ax.set_xlabel(r'$s^{\mathrm{IT2}}_{\mathrm{j}}$', fontsize=22)
        f2 = get_sets.load_data('results/it2/' + f2s[i] + '.pickle')
        ax.set_ylabel(r'$s^{\mathrm{IT2}}_{\mathrm{' + labels[i] + '}}$',
                      fontsize=22)
        plt.scatter(f1, f2)
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.savefig('results/it2/' + f2s[i] + '.pdf')
        plt.clf()
    # plot Bustince separtely as intervals
    f2 = get_sets.load_data('results/it2/bustince.pickle')
    ax = fig.gca()
    fig.subplots_adjust(bottom=0.15)
    ax.set_xlabel(r'$s^{\mathrm{IT2}}_{\mathrm{j}}$', fontsize=22)
    for i in range(len(f2)):
        ax.axvline(x=f1[i], ymin=float(f2[i][0]), ymax=float(f2[i][1]))
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    ax.set_ylabel(r'$s^{\mathrm{IT2}}_{\mathrm{b}}$', fontsize=22)
    plt.savefig('results/it2/bustince.pdf')


def bustince_correlation():
    """Calculate the correlation between the 
       lower and upper intervals of Bustince results with Jaccard.
    """
    J = get_sets.load_data('jaccard_it2.pickle')
    B = get_sets.load_data('bustince.pickle')
    J = [float(j) for j in J]
    print stats.pearsonr(J, [float(b[0]) for b in B])
    print stats.pearsonr(J, [float(b[1]) for b in B])


if __name__ == '__main__':
    #save_all_results()
    plot_all_figures()
    #bustince_correlation()

