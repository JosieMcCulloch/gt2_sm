from decimal import Decimal
import matplotlib.pyplot as plt
from scipy.stats import rankdata

from fuzzycreator.fuzzy_sets.discrete_t1_fuzzy_set import DiscreteT1FuzzySet
from fuzzycreator.measures import similarity_t1
from fuzzycreator.measures import distance_t1
from fuzzycreator import global_settings as gs
from fuzzycreator import visualisations


gs.global_uod = [0, 1]
gs.global_x_disc = 101
gs.global_alpha_disc = 10
gs.set_rounding(6)


def set_value(x):
    return Decimal(x).quantize(gs.DECIMAL_ROUNDING)


def load(filepath):
    """Return list of {x,y} dicts from csv filepath that details fuzzy sets."""
    fsock = open(filepath, 'r')
    firstline = fsock.readline().strip().split(',')
    total_sets = len(firstline) - 1
    set_points = [{set_value(firstline[0]):set_value(y)} for y in firstline[1:]]
    for line in fsock:
        values = line.strip().split(',')
        for i in range(1, total_sets+1):
            set_points[i-1][set_value(values[0])] = set_value(values[i])
    fsock.close()
    return [DiscreteT1FuzzySet(p) for p in set_points]


def similarities(sets, function):
    results = []
    total = len(sets)
    for i in range(total):
        print i
        for j in range(i, total):
            results.append(function(sets[i], sets[j]))
    return results



def plot_all_figures():
    fig = plt.figure()
    sets = load('L1_t1.csv')
    f1 = similarities(sets, similarity_t1.jaccard)
    f2 = similarities(sets, similarity_t1.pappis2)
    ax = fig.gca()
    fig.subplots_adjust(bottom=0.15)
    ax.set_xlabel(r'$s^{\mathrm{T1}}_{\mathrm{j}}$', fontsize=22)
    ax.set_ylabel(r'$s^{\mathrm{T1}}_{\mathrm{p}}$', fontsize=22)
    plt.scatter(f1, f2)
    #non scatter plot
    #s = sorted(f1)
    #X = []
    #Y = []
    #for j in s:
    #    X.append(j)
    #    Y.append(f2[f1.index(j)])
    #plt.plot(X, Y)
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.show()
    



if __name__ == '__main__':
    plot_all_figures()
    
