import cPickle as pickle
from decimal import Decimal
import numpy as np

from fuzzycreator.membership_functions.gaussian import Gaussian
from fuzzycreator.fuzzy_sets.interval_t2_fuzzy_set import IntervalT2FuzzySet
from fuzzycreator.fuzzy_sets.general_t2_fuzzy_set import GeneralT2FuzzySet
from fuzzycreator import global_settings as gs
from fuzzycreator import visualisations


gs.global_uod = [0, 1]
gs.global_x_disc = 101
gs.global_alpha_disc = 10
gs.set_rounding(4)


def how_many_disjoint(sets):
    """Counts how many pairs of fuzzy sets are disjoint.

    Returns tuple (total disjoint, total pairs).
    """
    n = 0
    t = 0
    total_sets = len(sets)
    for i in range(total_sets):
        i_min = sets[i].mf2.x_min
        i_max = sets[i].mf2.x_max
        for j in range(i+1, total_sets):
            t += 1
            j_min = sets[j].mf2.x_min
            j_max = sets[j].mf2.x_max
            if (i_max < j_min) or (i_min > j_max):
                n += 1
    print n, t


def generate_locs_and_scales(uod, var, samples):
    """Return randomly generated means and std devs.

    uod: Range of x values locs fall between
    samples: Total locs and scaled generated
    Returns two lists [locs] [scales]"""
    def _rescale(c, rng):
        return round(c * (rng[1] - rng[0]) + rng[0], 2)
    locs = [_rescale(x, uod) for x in np.random.random(samples)]
    # Make scale no smaller than 0.025 to avoid issues rounding to 1dp
    scales = [max(0.025, _rescale(x, var)) for x in np.random.random(samples)]
    return locs, scales


def generate_sets_it2():
    """Return fuzzy sets with Gaussian functions."""
    sets = []
    locs, scales = generate_locs_and_scales([0, 1], [0, 0.2], 30)
    for sample_i in range(30):
        lmf = Gaussian(locs[sample_i], scales[sample_i], 1.0)
        umf = Gaussian(locs[sample_i], scales[sample_i], 0.7)
        sets.append(IntervalT2FuzzySet(lmf, umf))
    save_data(sets, 'fuzzy_sets/sets_it2.pickle')


def generate_sets_gt2():
    """Return fuzzy sets with Gaussian functions.

    Note: IT2 FSs must already have been created.
    """
    sets = []
    it2_sets = load_data('fuzzy_sets/sets_it2.pickle')
    for sample_i in range(30):
        sets.append(GeneralT2FuzzySet(it2_sets[sample_i].mf1,
                                      it2_sets[sample_i].mf2))
    save_data(sets, 'fuzzy_sets/sets_gt2.pickle')


def load_data(filename):
    fsock = open(filename, 'r')
    sims = pickle.load(fsock)
    fsock.close()
    return sims


def save_data(data, filename):
    fsock = open(filename, 'wb')
    sims = pickle.dump(data, fsock)
    fsock.close() 

